# -*- coding: utf-8 -*-
"""
Created on Fri Feb 06 11:49:10 2015

@author: Jyldyz
"""

import requests
from lxml import etree
import os

#you may need to change your woking directory to project folder:
os.chdir("C:/Users/Jyldyz/Documents/Python Scripts/job_scraping")

#the list of cicites was generated from Wikipedia entry on the NY state
cities = open("nycities.csv", "r")

#the string after q= in the link works as a search term
#change the publisher parameter to your publisher ID provided by Indeed.com
fileUrl_base = "http://api.indeed.com/ads/apisearch?publisher=000000000000000&q="


#loc = "l=manchester&" #location (use a postal code or a "city, state/province/region" combination)
sorting = "sort=" #Sort by relevance or date. Default is relevance.
radius = "radius=" #Radius from location
site_type = "st=" # For job boards use "jobsite". For direct employer websites use "employer"
job_type = "jt=" #Allowed values: "fulltime", "parttime", "contract", "internship", "temporary".
start = "start=" #Start results at this result number, beginning with 0. Default is 0.
limit = "limit=20" #Maximum number of results returned per query. Default is 10
days_back = "fromage=" #Number of days back to search
filtr_dup = "filter=1" #Filter duplicate results. 0 turns off duplicate job filtering. Default is 1.
geo = "latlong=1" #If latlong=1, returns latitude and longitude. Default is 0.
country = "co=us" #Search within country specified. Default is us 
channel = "chnl=" #Group API requests to a specific channel
user_ip = "userip=31.49.29.166" # Required. End-user IP address
user_agent = "useragent=Chrome/40.0.2214.111"
version = "v=2" # End-user browser

results = {}
all_jobs = []

#example shown here scrapes jobs for multiple cities, however you can run the code 
#for a single city, just comment out top two lines in the segment and assingn
#the city of interest to city[0] 

#looping through all cities with open("swdata_cities.csv", "rb") as swdata:
for c in cities:
    city = c.split(",")
    if city[0] == "Ithaca":
        loc = "l="+ city[0] + "%2C+ny"   
        fileUrl = '&'.join([fileUrl_base,loc,sorting,radius,site_type,job_type,start,limit,days_back,\
        filtr_dup,geo,country,channel,user_ip,user_agent,version])

#send request to indeed.com
        r = requests.get(fileUrl)
        xml = r.content
        root = etree.fromstring(xml)

#count total results and write to a dictionary    
        tr = root[4]
        for r in tr.iter():
            results[city[0]] = int(r.text)

#based on the number of total results loop through cities and pages   
for entry in results:
    loc = "l="+ entry + "%2C+ny"
    pages = range(0, results[entry], 20)
    for p in pages:
        start2 = "start="+ str(p)
        fileUrl2 = '&'.join([fileUrl_base,loc,sorting,radius,site_type,job_type,start2,limit,days_back,\
        filtr_dup,geo,country,channel,user_ip,user_agent,version])

#parse xml and extract details for each job
        detail = requests.get(fileUrl2)
        xml2 = detail.content
        root2 = etree.fromstring(xml2)        
        b = root2[9] # this is "results" element that contains most information

#write job details to a dictionary and then collect all jobs in a list      
        for element in b:
            each_job = {} 
            for job in element.iter():
                each_job[job.tag] = job.text
            all_jobs.append(each_job)

#monitor progress    
    print entry
    
#if you are looping through multiple cities, you may end up with overlapping
#job postings because of 25 mile radius setting
#this is why you may need to get unique entries out of the resulting all jobs list
#in my origianl project I used ScraperWiki platform and scraperwiki library
#to write data to sql database and eliminate duplicates
#ScraperWiki platform has also allowed me to export the sql table in csv format

