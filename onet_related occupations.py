# -*- coding: utf-8 -*-
"""
Created on Wed Mar 18 10:15:04 2015

@author: Jyldyz
"""

import requests
from lxml import etree
import csv
import pickle
import os

#you may need to set your working directory to a project folder:
os.chdir("C:/Users/Jyldyz/Documents/Python Scripts/job_scraping")

#open the variations dictionary, which is the dictionary of ONET occupations, where
#dictionary key is ONET soc code and values are known job titles.
with open("variations.pkl", "rb") as infile:
    variations = pickle.load(infile)

#build urls to access info on related occupations
#the link and authorization details are available via ONET API documentation
url_base = "https://services.onetcenter.org/ws/online/occupations/"
#soc_code = "17-2051.00"
related = "/summary/related_occupations"

#loop through occupations and extract data on related occupations  
all_related = []
for v in variations:
    job_related = []
    fileUrl = "".join([url_base, v, related])
    r = requests.get(fileUrl,auth=('user', 'password'))
    xml = r.content
    root = etree.fromstring(xml)

    for i in root:
        related_occupation = []
        for x in i.iter(["code", "title"]):
            related_occupation.append(x.text)
        job_related.append(related_occupation)
    
    all_related.append([v, job_related])
    
#flatten the related occupations list to write to csv
all_related_flat = []
for al in all_related:
    code = al[0]
    data = al[1]
    for d in data:
        all_related_flat.append([code, d[0], d[1]])    
       
with open("all_related.csv", "wb") as output:
    f = csv.writer(output, delimiter = ",") 
    f.writerows(all_related_flat)