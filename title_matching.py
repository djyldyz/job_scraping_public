# -*- coding: utf-8 -*-
"""
Created on Fri Feb 06 11:49:10 2015

@author: Jyldyz
"""

import os
from fuzzywuzzy import fuzz
import csv
import pickle


#this script is used to assign actual job titles to an appropriate ONET occupation


#you may need to set your working directory to a project folder:
os.chdir("C:/Users/Jyldyz/Documents/Python Scripts/job_scraping")

#function to compare string to multiple strings in a list, find the maximum 
#ratio. If there are ties use length of the string in the list as a second criteria
#assumption is that for equal match ratios, the longer string indicates the better match

def getBestSingle(t,r):
    matches = []
    m = fuzz.token_set_ratio(t, r)
    matches.append([r,m])
    def getKey(item):
        return (item[1], len(item[0]))
    res = max(matches, key=getKey)
    return res

def getKey(item):
        return (item[1], len(item[0]))

#function to compare a string to a dictionary, where each dictionary value is a list
def getBestSingleDict(t,d):
    def getKey1(item):
        return (item[2], len(item[1]))
    best_matches = []
    for i in d:
        for l in d[i]:
            ref_string, match_score = getBestSingle(t, l)
            best_matches.append([i, ref_string, match_score])
    res = max(best_matches, key=getKey1)
    return res

#open the csv file that was generated from an sql database on the Scraperwiki
#platform, which I used to scrape data initially:
actual_jobs = []
with open("swdata_cities2.csv", "rb") as swdata:
    data = csv.reader(swdata)
    next(data, None)
    for d in data:
        actual_jobs.append([d[15], d[17]])


#open the variations dictionary, which is the dictionary of ONET occupations, where
#dictionary key is ONET soc code and values are known job titles.
with open("variations.pkl", "rb") as infile:
    variations = pickle.load(infile)


#step 1 exact match to first title in dictionary 
title_to_code2 = []
no_match = []
for j in actual_jobs:
    match = None
    for v in variations:
        if j[0] in variations[v]:
            title_to_code2.append([j[1], v, variations[v][0], j[0]])
            match = 1
            break
            
    if match == None:
        no_match.append(j)

#step 2 exact match to title in look-up table
#the look-up table is the result of the previous run of the code and represents
#a list of tuples (actual job titles assigned to appropriate ONET occupations)

with open("look_up.pkl", "rb") as infile:
    look_up = pickle.load(infile)

matched = []
for n in no_match:
    for lu in look_up:
        if n[0] in lu:
            title_to_code2.append([n[1], lu[0], lu[1], n[0]])
            matched.append(n)
            break

#update no_match list to delete the entries that have been assigned to an occupation
no_match2 = []
for n in no_match:
    if n in matched:
        continue
    else:
        no_match2.append(n)



#step 3 run the getBestSingle against matched actual titles
#in the look-up table
#criteria: match ratio of at least 95  
for num, nm in enumerate(no_match2):
    test3 = []    
    for ix, lu in enumerate(look_up):
        match = getBestSingle(nm[0], lu[2])
        test3.append([match[0], match[1], lu[0]])        
        best_match = max(test3, key = getKey)
    if best_match[1] >=95:
        title_to_code2.append([nm[1], best_match[2], variations[best_match[2]][0],nm[0]])
        matched.append(nm)
        print num

no_match3 = []
for n in no_match2:
    if n in matched:
        continue
    else:
        no_match3.append(n)



#step 4 run the getBestDict against all known titles in the variations dictionary
#criteria: match ratio of at least 95  
for nm in no_match3:
    try:
        match = getBestSingleDict(nm[0], variations)
        if match[2] >= 95:
            title_to_code2.append([nm[1], match[0], variations[match[0]][0], nm[0]])
            matched.append(nm)
    except:
        continue

no_match4 = []
for n in no_match3:
    if n in matched:
        continue
    else:
        no_match4.append(n)
        
#optional step manual assignment
#you can expand this as necessary if certain jobs are common in the list
for nm in no_match4:
    if "drivers" in nm[0].lower() or "truck driving" in nm[0].lower():
        title_to_code2.append([nm[1], '53-3033.00', variations['53-3033.00'][0], nm[0]])
        matched.append(nm)
    if "desktop support" in nm[0].lower():
        title_to_code2.append([nm[1], '15-1151.00',variations['15-1151.00'][0], nm[0]])
        matched.append(nm)
    if "retail associate" in nm[0].lower() or "store associate" in nm[0].lower():
        title_to_code2.append([nm[1], '41-2031.00', variations['41-2031.00'][0], nm[0]])
        matched.append(nm)
    if "logistics" in nm[0].lower():
        title_to_code2.append([nm[1], '13-1081.00',variations['13-1081.00'][0], nm[0]])
        matched.append(nm)
    if "merchandising" in nm[0].lower():
        title_to_code2.append([nm[1], '27-1026.00', variations['27-1026.00'][0], nm[0]])
        matched.append(nm)
    if "stock" in nm[0].lower():
        title_to_code2.append([nm[1], '43-5081.03', variations['43-5081.03'][0], nm[0]])
        matched.append(nm)
    if "tire tech" in nm[0].lower():
        title_to_code2.append([nm[1], '49-3093.00', variations['49-3093.00'][0],  nm[0]])
        matched.append(nm)
    if "pharmacy associate" in nm[0].lower():
        title_to_code2.append([nm[1], '31-9095.00', variations['31-9095.00'][0], nm[0]])
        matched.append(nm)

no_match5 = []
for n in no_match4:
    if n in matched:
        continue
    else:
        no_match5.append(n)

##optional step lenient assignment then checking manually
##for the remaining unmatched get the best match ratio of job title and known titles, 
##then check manually in csv
#test_nomatch = []
#for nm in no_match5:
#    match = getBestSingleDict(nm[0], variations)
#    test_nomatch.append([nm[1], match[0], variations[match[0]][0], nm[0]])
#
#
#with open("test_nomatch.csv", "w") as outfile:
#    for tnm in test_nomatch:
#        outfile.write(",".join(['"'+a+'"' for a in tnm])+"\n")
##review the csv to check matching, make edits as necessary, only after that
##load it back
#
#
##import the reviewed csv table, which contains the manually matched job titles
#last_match = []
#with open("test_nomatch.csv", "rb") as testdata:
#    data = csv.reader(testdata)
#    for d in data:
#        last_match.append([d[0], d[1], d[2], d[3]])
#
##add to the overall list of matched titles
#for lm in last_match:
#    title_to_code2.append(lm)

#write to csv file to use in Tableau
with open("title_to_code2.csv", "wb") as output:
    f = csv.writer(output, delimiter = ",") 
    f.writerows(title_to_code2)
