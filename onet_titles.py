# -*- coding: utf-8 -*-
"""
Created on Wed Mar 18 10:15:04 2015

@author: Jyldyz
"""

import requests
from lxml import etree
import csv
import os
import pickle

#you may need to change your woking directory to project folder:
os.chdir("C:/Users/Jyldyz/Documents/Python Scripts/job_scraping")

#import soc_code list, which contains ONET soc_codes and titles
with open("soc_code.pkl", "rb") as infile:
    soc_code = pickle.load(infile)

#build an url to access occupation summary info
#the link and authorization details are available via ONET API documentation
url_base = "https://services.onetcenter.org/ws/online/occupations/"
#soc_code = "17-2051.00"

#loop through soc_codes to extract information on occupation titles
all_titles = []
for sc in soc_code:  
    fileUrl = "".join([url_base, sc])
    
    r = requests.get(fileUrl,auth=('user', 'password'))
    xml1 = r.content
    root1 = etree.fromstring(xml1)

   #extracting occupation title 
    first_title = root1[1]
    ft = []
    for element in first_title.iter("title"):
        ft.append(element.text)
    
    #extracting the sample of reported job titles    
    try:
        titles = root1[4]
        if titles.tag == "sample_of_reported_job_titles":                
            longer = []
            parallel = range(len(titles))
            for ix in parallel:
                for num in titles[ix].iter("title"):
                    longer.append([num.text])
                    
                all_titles.append([sc, ft[0], longer])
        else:
            all_titles.append([sc, ft[0], ["None"]])
    except:
        print (sc, "no data")

#processing the titles list to make it flat    
all_titles_flat = []    
for t in all_titles:
    code = t[0]
    title = t[1]
    other = t[2]
    for o in other:
        all_titles_flat.append([code, title, o])
        
#extract only strings without "[" and "]" symbols
all_titles_clean = []
for at in all_titles_flat:
    other = at[2]
    all_titles_clean.append([at[0], at[1], other[0]])

#write to csv to use in Tableau workbook
with open("all_titles.csv", "wb") as output:
    f = csv.writer(output, delimiter = ",") 
    f.writerows(all_titles_clean)


#create a list from the sample of reported job titles
#the list contains of flat lists[soc_code, occupation title, reported job titles] 
sample_titles = []
for at in all_titles:
    job_titles = []
    data = at[2]
    job_titles.append(at[0])
    job_titles.append(at[1])
    for d in data:
        job_titles.append(d[0])
    sample_titles.append(job_titles)
      
#use the list of reported job titles to create an occupation dictionary
#dictionary key is ONET soc code and values are job titles.
#this dictionary is later used extensively for looping and title matching
variations = {}
for st in sample_titles:
    variations[st[0]] = st[1:]
    
#correct for jobs that don't have any other titles
for v in variations:
    if variations[v][1] == "N":
        variations[v][1] == "None"
        print (v, "done")

#create entry for any other occupations
variations['60-0000.00'] = "Other"

#store for future use
with open("variations.pkl", "wb") as outfile:
    pickle.dump(variations, outfile)
    