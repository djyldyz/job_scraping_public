# -*- coding: utf-8 -*-
"""
Created on Sun Feb 08 20:55:19 2015

@author: Jyldyz
"""

#import os
import sqlite3

db_filename = 'onetdata.db'
conn = sqlite3.connect(db_filename)
cursor = conn.cursor()

## this is the function that will read an sql table
#def readData(f):
#    p = os.path.join(os.path.curdir, "db_19_0_mysql",f)    
#    openf = open(p, 'r')
#    data = openf.read()
#    return data

## we use the readData function to read all tables in the folder using a loop
#folder = os.listdir("C:/Users/Jyldyz/Documents/Python Scripts/db_19_0_mysql")
#for i in folder:
#    sql = readData(i)
#    cursor.executescript(sql)


# get list of tables
cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
result = cursor.fetchall()

## test query to fetch data from occupation_data table
#cursor.execute("""
#select * from occupation_data where onetsoc_code = '17-2072.00' or onetsoc_code = '29-1066.00'
#""")
#result = cursor.fetchall()

# read infor about column names and structure of the table
cursor.execute("""
PRAGMA table_info(occupation_data)
""")
result = cursor.fetchall()


conn.close()
